# cotentful
* contentful提供标准RESTful JSON API供调用。免费版本共提供3个用户、三种角色：管理员、编辑者、浏览者。用户与角色之间可以为多种匹配关系。限制每秒最多10次访问量。
* cotentful中的组织形式如下：  
  *space，database的概念。（每个space可对应一个app，DataHub已经申请一个名为DataHub的space。  
  
   *每个space下面可定义多个contentype，cotentype类似于schema的概念。意为定义每个space下有几种记录形式。如对DataHub，可定义数据需求、数据源信息、产品信息等。  
   
   *每个content type可以定义下面field，field类似于字段的概念，详细订一个每个conten type包含哪些字段，字段的格式要求等。没type下的field不超过50个，type内的每个field id 唯一。   
   
  Content type properties:
   <table><tr><td>
Field</td><td>	Type	</td><td>Description</td></tr>
<tr><td>sys	</td><td>Sys</td><td>	Common system properties.</tr>
<tr><td>name	</td><td>String</td><td>	Name of the content type.</tr>
<tr><td>description	</td><td>String	</td><td>Description of the content type.</tr>
<tr><td>fields</td><td>	[Field]</td><td>	List of fields.</tr>
<tr><td>displayField</td><td>	String	</td><td>ID of main field used for display.</tr></table>


	field type description
	<table>
<tr><td>Field</td><td>	Type</td><td>	Description</tr>
<tr><td>id	</td><td>String</td><td>	The id of a field corresponds to a key in the fields property of entries with this content type. It must be unique among all fields in the content type.
</tr><tr><td>name	</td><td>String	</td><td>The name of the field is the human readable label that will be used for this field in the Contentful web app.
</tr><tr><td>type	</td><td>String	</td><td>The type of the field determines what data can be stored here, as well was what query operations you can perform with this field. See below for more details.
</tr><tr><td>items</td><td>	Schema	</td><td>Defines a subschema for the elements of an array field. This is required when type is "Array".
</tr><tr><td>required</td><td>	Boolean	</td><td>Describes whether the field is mandatory.
</tr><tr><td>localized</td><td>	Boolean</td><td>	Describes whether the field will support different values for different locales.
</tr><tr><td>disabled</td><td>	Boolean	</td><td>Describes whether the field is disabled. Disabled fields are hidden in the editing application.
</tr><tr><td>omitted</td><td>	Boolean	</td><td>If set to true fully omits this field in the Content Delivery API and Preview API responses. The Content Management API is not affected by this.
</tr>
</table>  

	field type与JSON type的对应关系
	<table>
	<tr><td>Name</td><td>	JSON Type</td><td>		Description	Example</td></tr>	
<tr><td>Symbol	</td><td>String	</td><td>Basic list of characters. Maximum length is 256.	"The title"</td></tr>
<tr><td>Text</td><td>String	</td><td>Same as symbol, but filterable via full-text search. Maximum length is 50,000.	" This is a post and ..."</td></tr>
<tr><td>
Integer</td><td>	Number	</td><td>Number type without decimals. Values from -253 to 253.	42</td></tr>
<tr><td>Number</td><td>	Number</td><td>	Number type with decimals.	3.14</td></tr>
<tr><td>
Date	</td><td>String</td><td>	Date/time in ISO 8601 </td></tr>
format.	"2015-11-06T09:45:27"</td></tr>
<tr><td>
Boolean	</td><td>Boolean	</td><td>Flag, true or false	true</td></tr>
<tr><td>
Location</td><td>	Object	</td><td>A geographic location specified in latitude and longitude.	{"lat":"52.5018616","lon":"13.4112619"}</td></tr>
<tr><td>
Link	</td><td>Object	</td><td>A reference to an entry or asset. The type of the referenced item is defined by the linkType property. See links for more information	{"sys": {"type": "Link", "linkType": "Entry", "id": "af35vcx8etbtwe8xv"}}</td></tr>
<tr><td>
Array</td><td>	Array</td><td>	List of values. See array fields below for details on what types of values can be stored in an array.	["name1", "name2", ...]</td></tr>
<tr><td>
Object	</td><td>Object</td><td>	Arbitrary Object.	{"somekey": ["arbitrary", "json"]}"</td></tr>
</table>
      
    *Entry，为每条记录，遵循之前定义的content type、field。Entry可以有链接，链接可指向别的entry，array等。Entries are represented as JSON data.
    
     *asset，综合文件库。图片、视频、ppt等其他形式的文件，可保存在array中。assets are represented as (binary) files.免费账号可以有100个asset.
 

## 1 Content Management API(用于写数据，包括新增、修改)
###Updating and version lockingContentful （版本更新的用法没有找到）
uses optimistic locking. When updating an existing resource its current version needs to be specified as the value of the X-Contentful-Version HTTP header (this header is automatically set when using our official SDKs). The version is compared with the resource's current version on the server to ensure that a client doesn't overwrite a resource that has since been updated. If the version changed in between the update will be rejected.

###1.1 错误代码
<table>
   <tr>
    <td>HTTP Status</td><td> Code Error Code  </td><td>Description</td>
    </tr>
    <tr>
	<td>400	</td><td>BadRequestError</td><td>The request was malformed or it is missing a required parameter.</td>
	</tr>
    <tr>
	<td>400	</td><td>InvalidQueryError	</td><td>The request contained invalid or unknown query parameters.</td>
	</tr>
    <tr>
	<td>401</td><td>	AccessTokenInvalidError	</td><td>The authorization token was invalid.</td>
	</tr>
    <tr>
	<td>403</td><td>	AccessDeniedError</td><td>	The user tried to access a resource that they do not have access to. This could include a missing role.</td>
	</tr>
    <tr>
	<td>404	</td><td>NotFoundError	</td><td>The requested resource or endpoint could not be found.</td>
	</tr>
    <tr>
	<td>422	</td><td>ValidationFailedError	</td><td>The triggered query references an invalid field.</td>
	</tr>
    <tr>
	<td>429	</td><td>RateLimitExceededError	</td><td>The user sends too many requests per second.</td>
	</tr>
    <tr>
	<td>500	</td><td>ServerError</td><td>	Something went wrong on our end.</td>
	</tr>
    <tr>
	<td>502	</td><td>AccessDeniedError	</td><td>The space has not been used for a long time and hence it has been hibernated, but it will reappear if the user begins using it again.</td>
	</tr>
	<tr>
	<td>409		</td><td>VersionMismatchError	</td><td>	This error happens when you're trying to update an existing asset, entry or content type, and you either didn't specify the current version of the object or specify an outdated version.</td></tr>
<tr>
<td>422	</td><td>	ValidationFailedError	</td><td>	The request payload was a valid JSON, but something was wrong with the data. Check the error details field – it should provide more specific information about the error.</td></tr>
<tr>
<td>422	</td><td>	InvalidEntryError	</td><td>	The entered value is invalid.</td></tr>
<tr>
<td>422	</td><td>	ValidationFailedError	</td><td>	The triggered query references an invalid field.</td></tr>
 </table>

###1.2 创建一个space（可在contentful页面操作）
POST https://api.contentful.com/spaces

request：  

	curl --include \
     --request POST \
     --header "Authorization: Bearer b4c0n73n7fu1" \
     --header "Content-Type: application/vnd.contentful.management.v1+json" \
     --header "X-Contentful-Organization: 1PLOOEmTI2S1NYald2TemO" \
     --data-binary "{
  \"name\": \"Example Space\",
  \"defaultLocale\": \"en\"
}" \
'https://api.contentful.com/spaces'response  

	HEADERS
	Content-Type:application/vnd.contentful.management.v1+json  
	
	BODY
	{
 	 "sys": {
  	  "type": "Space",
  	  "id": "cfexampleapi",
  	  "version": 3,
   	 "createdAt": "2015-05-18T11:29:46.809Z",
   	 "createdBy": {
    	  "sys": {
     	   "type": "Link",
      	  "linkType": "User",
       	 "id": "4FLrUHftHW3v2BLi9fzfjU"
    	  }
   	 },
    	"updatedAt": "2015-05-18T11:29:46.809Z",
    	"updatedBy": {
    	  "sys": {
     	   "type": "Link",
      	  "linkType": "User",
     	   "id": "4FLrUHftHW3v2BLi9fzfjU"
    	  }
  	  }
 	 },
 	 "name": "Contentful Example API",
 	 "defaultLocale": "en"
	}
###1.3  获取一个账户下的全部spaces（可在contentful页面操作）
GET https://api.contentful.com/spaces
request  

	curl --include \
     --header "Authorization: Bearer b4c0n73n7fu1" \
  'https://api.contentful.com/spaces'
	
response

	HEADERS
	Content-Type:application/vnd.contentful.management.v1+json
	BODY
	{
  	"sys": {
   	 "type": "Array"
 	 },
 	 "total": 0,
  	"skip": 0,
  	"limit": 100,
 	 "items": []
	}
###1.4 获取一个space（可在contentful页面操作）

GET https://api.contentful.com/spaces/space_id

request

	
	curl --include \
     --header "Authorization: Bearer b4c0n73n7fu1" \
 	 'https://api.contentful.com/spaces/fp91oelsziea'
  
response

	HEADERS
	Content-Type:application/vnd.contentful.management.v1+json
	BODY
	{
 	 "sys": {
  	  "type": "Space",
   	 "id": "cfexampleapi",
    	"version": 3,
   	 "createdAt": "2015-05-18T11:29:46.809Z",
   	 "createdBy": {
     	 "sys": {
     	   "type": "Link",
      	  "linkType": "User",
      	  "id": "4FLrUHftHW3v2BLi9fzfjU"
      	}
  	  },
  	  "updatedAt": "2015-05-18T11:29:46.809Z",
  	  "updatedBy": {
   	   "sys": {
    	    "type": "Link",
    	    "linkType": "User",
     	   "id": "4FLrUHftHW3v2BLi9fzfjU"
     	 }
   	 }
 	 },
	  "name": "Contentful Example API",
 	 "defaultLocale": "en"
	}



###1.5获取指定content type（此接口可用于获取指定content type的系统信息，但是选择delivery相应的接口更好）
GET https://api.contentful.com/spaces/space_id/content_types/content_type_id

request  

	curl --include \
     	--header "Authorization: Bearer b4c0n73n7fu1" \
  	'https://api.contentful.com/spaces/fp91oelsziea/	content_types/3ORKIAOaJqQWWg86MWkyOs'
  	
response  

 	HEADERS
	Content-Type:application/	vnd.contentful.management.v1+json  
	
	BODY
	{
 	 "sys": {
  	  "type": "ContentType",
   	 "id": "3ORKIAOaJqQWWg86MWkyOs",
    	"space": {
    	  "sys": {
     	   "type": "Link",
      	  "linkType": "Space",
      	  "id": "fp91oelsziea"
     	 }
    	},
    	"version": 1,
   	 "createdAt": "2015-05-18T11:29:46.809Z",
   	 "createdBy": {
    	  "sys": {
     	   "type": "Link",
      	  "linkType": "User",
       	 "id": "4FLrUHftHW3v2BLi9fzfjU"
      	}
   	 },
   	 "updatedAt": "2015-05-18T11:29:46.809Z",
    "updatedBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "4FLrUHftHW3v2BLi9fzfjU"
      }
    }
  }
}

###1.6 激活一个contentytpe（可在contentful页面操作）

PUT https://api.contentful.com/spaces/space_id/content_types/content_type_id/published  

request  

	curl --include \
     --request PUT \
     --header "Authorization: Bearer b4c0n73n7fu1" \
     --header "X-Contentful-Version: 11" \
 	 'https://api.contentful.com/spaces/fp91oelsziea/	content_types/3ORKIAOaJqQWWg86MWkyOs/published'
 	 
response  

	HEADERS
	Content-Type:application/vnd.contentful.management.v1+json
	BODY
	{
 	 "name": "Blog Post",
  	"fields": [
  	  {
      "id": "title",
      "name": "Title",
      "required": true,
      "localized": true,
      "type": "Text"
    },
    {
      "id": "body",
      "name": "Body",
      "required": true,
      "localized": true,
      "type": "Text"
   	 }
 	 ],
 		 "sys": {
  	 	 "firstPublishedAt": "2015-05-15T13:38:11.311Z",
   	 "publishedCounter": 2,
   	 "publishedAt": "2015-05-15T13:38:11.311Z",
   	 "publishedBy": {
     	 "sys": {
    	    "type": "Link",
    	    "linkType": "User",
    	    "id": "4FLrUHftHW3v2BLi9fzfjU"
 	     }
 	   },
	    "publishedVersion": 9
 	 }
	}
###1.7 获取一个space下的所有激活状态的content信息
GET https://api.contentful.com/spaces/space_id/public/content_types
此处space id：hb6df7rxvqfj
access token：2d78ec8e0a0b199bb6003159557d13d056c466614dd32b696c10030a7459544a

request   
 
	curl --include \
 	    --header "Authorization: Bearer b4c0n73n7fu1" \
	  'https://api.contentful.com/spaces/fp91oelsziea/	public/content_types'
	  
respond
	每个content的具体信息，包括每个space的描述。

	
###1.7 删除一个content type（可在contentful页面操作）

DELETE https://api.contentful.com/spaces/space_id/content_types/content_type_id/published
  
 request  
 
 		curl --include \
   	  --request DELETE \
    	 --header "Authorization: Bearer b4c0n73n7fu1" \
  	'https://api.contentful.com/spaces/fp91oelsziea/	content_types/3ORKIAOaJqQWWg86MWkyOs/published'
  	
 response    
 
	HEADERS  
	Content-Type:application/		vnd.contentful.management.v1+json
	BODY
	{
 		 "name": "Blog Post",
  		"fields": [
  	  {
      "id": "title",
      "name": "Title",
      "required": true,
      "localized": true,
      "type": "Text"
    },
    		{
      "id": "body",
      "name": "Body",
      "required": true,
      "localized": true,
      "type": "Text"
    	}
 	 ],
  	"sys": {
  	  "firstPublishedAt": "2015-05-15T13:38:11.311Z",
   	 "publishedCounter": 2
 	 }
	}
###1.8 获取一个space下的全部entries（用delivery对应的接口代替，速度更快）
GET https://api.contentful.com/spaces/space_id/entries

request  

	curl --include \
     --header "Authorization: Bearer b4c0n73n7fu1" \
  	'https://api.contentful.com/spaces/fp91oelsziea/	entries'
  	
 response  
 
 	Content-Type:application/	vnd.contentful.management.v1+json
	BODY
	{	
  	"sys": {
   	 "type": "Array"
  	},
  	"total": 0,
  	"skip": 0,
 	 "limit": 100,  #此处分页可选每页多少条。
  	"items": []
	}
###1.9 新增一条记录（此接口需要用到）
POST https://api.contentful.com/spaces/space_id/entries  

access token:  
4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a
space_id:  
hb6df7rxvqfj

request    

	curl --include \
    	 --request POST \
    	 --header "Authorization: Bearer b4c0n73n7fu1" \
     	--header "Content-Type: application/	vnd.contentful.management.v1+json" \
 	    --header "X-Contentful-Content-Type: 	hfM9RCJIk0wIm06WkEOQY" \
  	   --data-binary "{
  	\"fields\": {
   	 \"title\": {
   	   \"en-US\": \"Hello, World!\"
  	  },
   	 \"body\": {
    	  \"en-US\": \"Bacon is healthy!\"
   	 }
 	 }
	}" \
	'	https://api.contentful.com/spaces/fp91oelsziea/	entries'
response（除了新增的记录之外，返回包含sys、space、content信息，非常丰富。）

	
	HEADERS
	Content-Type:application/vnd.contentful.management.v1+json
	BODY
	{
 	 "fields": {
 	   "title": {
  	    "en-US": "Hello, World!"
  	  },
 	   "body": {
 	     "en-US": "Bacon is healthy!"
 	 	  }
 	 },
 	 "sys": {
  	  "id": "helloworld",
  	  "type": "Entry",
  	  "version": 1,
   	 "space": {
   	   "sys": {
    	    "type": "Link",
    	    "linkType": "Space",
    	    "id": "fp91oelsziea"
   	   }
    },
    "contentType": {
      "sys": {
        "type": "Link",
        "linkType": "ContentType",
        "id": "hfM9RCJIk0wIm06WkEOQY"
      }
    },
    "createdAt": "2015-05-18T11:29:46.809Z",
    "createdBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "4FLrUHftHW3v2BLi9fzfjU"
      }
    },
    "updatedAt": "2015-05-18T11:29:46.809Z",
    "updatedBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "4FLrUHftHW3v2BLi9fzfjU"
     	 }
   		 }
 	 }
	}
###1.10 查询某一条记录(看起来很好，但不知道怎么用，如何获取entry-id)
GET https://api.contentful.com/spaces/space_id/entries/entry_id  

request  (这个不方便，需要知道记录对应的id。)

	curl --include \
    	 --header "Authorization: Bearer b4c0n73n7fu1" \
  	'https://api.contentful.com/spaces/fp91oelsziea/	entries/helloworld'
  
 response   
 
	HEADERS
	Content-Type:application/vnd.contentful.management.v1+json
	BODY
	{
 	 "fields": {
  	  "title": {
   	   "en-US": "Hello, World!"
    	},
   	 "body": {
   	   "en-US": "Bacon is healthy!"
  	  }
 	 },
 	 "sys": {
 	   "id": "helloworld",
  	  "type": "Entry",
  	  "version": 1,
  	  "space": {
    	  "sys": {
   	     "type": "Link",
    	    "linkType": "Space",
   	     "id": "fp91oelsziea"
  	    }
  		  },
    "contentType": {
      "sys": {
        "type": "Link",
        "linkType": "ContentType",
        "id": "hfM9RCJIk0wIm06WkEOQY"
      }
    },
    "createdAt": "2015-05-18T11:29:46.809Z",
    "createdBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "4FLrUHftHW3v2BLi9fzfjU"
      }
    },
    "updatedAt": "2015-05-18T11:29:46.809Z",
    "updatedBy": {
      "sys": {
        "type": "Link",
        "linkType": "User",
        "id": "4FLrUHftHW3v2BLi9fzfjU"
     	 }
    	}
  	}
	}
###1.11 删除某一条记录
DELETE  https://api.contentful.com/spaces/space_id/entries/entry_id
request

	curl --include \
     --request DELETE \
     --header "Authorization: Bearer b4c0n73n7fu1" \
     --header "X-Contentful-Version: 4" \
 	 'https://api.contentful.com/spaces/fp91oelsziea/entries/helloworld'
 	 
response  

# 查询接口
###2.1 查询某个Space下的所有记录（本次需要用到）
GEThttps://cdn.contentful.com/spaces/space_id/entries?access_token=access_token  

space id：hb6df7rxvqfj  

token：4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a  

request（此处的space id 、token已经是真实的值）

	curl --include \
	'https://cdn.contentful.com/spaces/hb6df7rxvqfj/entries?access_token=4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a'
  
 respond(此处按照每条记录返回，一条记录以field开始，下分别显示各个字段的值，下一个field为下一下记录，另外有一些sys信息，有需要时才需要关注)
 
 	{
  	"sys": {
   	 "type": "Array"
 	 },
		  "total": 2,
 	 "skip": 0,
 	 "limit": 100,
  	"items": [
  	  {
    	  "sys": {
     	   "space": {
      	    "sys": {
         	   "type": "Link",
         	   "linkType": "Space",
        	    "id": "hb6df7rxvqfj"
      	    }
      	  },
     	   "id": "49c4rCUef62wU8Ec4wcU8m",
     	   "type": "Entry",
     	   "createdAt": "2016-05-24T06:13:45.227Z",
      	  "updatedAt": "2016-05-24T06:13:45.227Z",
      	  "revision": 1,
      	  "contentType": {
       	   "sys": {
        	    "type": "Link",
        	    "linkType": "ContentType",
         	   "id": "DataHub"
        	  }
      	  },
        "locale": "zh-CN"
     	 },
      "fields": {
        "demand": [
          {
            "sys": {
              "type": "Link",
              "linkType": "Entry",
              "id": "4EFpZcoZVu08qKEs4mmQag"
            }
          }
        ],
        "require": "主题是测试中文是否能支持得足够识别。"
      }
    },
    {
      "sys": {
        "space": {
          "sys": {
            "type": "Link",
            "linkType": "Space",
            "id": "hb6df7rxvqfj"
          }
        },
        "id": "2MHFKRLVjO8UeI0Aikycci",
        "type": "Entry",
        "createdAt": "2016-05-06T06:24:31.275Z",
        "updatedAt": "2016-06-01T05:36:04.274Z",
        "revision": 2,
        "contentType": {
          "sys": {
            "type": "Link",
            "linkType": "ContentType",
            "id": "DataHub"
          }
        },
        "locale": "zh-CN"
      },
      "fields": {
        "demand": [
          {
            "sys": {
              "type": "Link",
              "linkType": "Entry",
              "id": "2MHFKRLVjO8UeI0Aikycci"
            }
          }
        ],
        "require": "### 数据内容描述\n用户换机行为数据。\n### 数据范围\n全国。",
        "boolean": true,
        "media": [
          {
            "sys": {
              "type": "Link",
              "linkType": "Asset",
              "id": "2XdrYx7qWsi2mKCEyQmUgO"
            }
          }
        ]
      }
    }
  	],
	
####2.2 查询一条记录（需要知道具体entryid 才能查询）
GET https://cdn.contentful.com/spaces/space_id/entries/entry_id?access_token=access_token

其中：
space id：hb6df7rxvqfj
token：4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a


request  

	curl --include \
	'https://cdn.contentful.com/spaces/cfexampleapi/entries/nyancat?access_token=b4c0n73n7fu1'


####2.3 查找某一种类型的content（本次用于查询）
GET https://cdn.contentful.com/spaces/space_id/entries?token:access_token=access_token&content_type=content_type

其中：
space id：hb6df7rxvqfj
token：4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a


request  

	curl --include \
	'https://cdn.contentful.com/spaces/cfexampleapi/	entries?access_token=b4c0n73n7fu1&content_type=cat'



 “###2.4 查找记录，全文检索包含的某个关键字（可是调用结果不对，不知为啥）
 GET https://cdn.contentful.com/spaces/space_id/entries?access_token=access_token&query=value
 
 其中：
space id：hb6df7rxvqfj  

token：4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a

request  

	curl --include \
	'https://cdn.contentful.com/spaces/cfexampleapi/
	entries?access_token=b4c0n73n7fu1&query=bacon'  ”

###2.4 获取单个asset（图片、附件等，需要知道asset_id）
GET https://cdn.contentful.com/spaces/space_id/assets/asset_id?access_token=access_token

其中：
space id：hb6df7rxvqfj  
asset_id：5zz2Z5Hn20y0Og86E6sQya  
token：4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a

request  （此处的space id 、asset_id、token已经是真实的值）

	curl --include \
	'https://cdn.contentful.com/spaces/hb6df7rxvqfj/assets/5zz2Z5Hn20y0Og86E6sQya?access_token=4a8d819fa51b568e23f5f8611b17de43f16d980887d3f4699b5a3af283a40a1a'


 respond
 
 	{
	  "sys": {
	    "space": {
	      "sys": {
	        "type": "Link",
	        "linkType": "Space",
	        "id": "hb6df7rxvqfj"
	      }
	    },
	    "id": "5zz2Z5Hn20y0Og86E6sQya",
	    "type": "Asset",
	    "createdAt": "2016-06-02T17:08:22.765Z",
	    "updatedAt": "2016-06-02T17:08:22.765Z",
	    "revision": 1,
	    "locale": "zh-CN"
	  },
	  "fields": {
	    "title": "西美LOGO",
	    "file": {
	      "url": "//images.contentful.com/hb6df7rxvqfj/5zz2Z5Hn20y0Og86E6sQya/fdfdb07d08aaab3e96371f7d1f6730a3/______LOGO.png",
	      "details": {
	        "size": 15061,
	        "image": {
	          "width": 160,
	          "height": 144
	        }
	      },
	      "fileName": "西美LOGO.png",
	      "contentType": "image/png"
	    }
	  }
	}
 

###2.5 按照发布日期排序或按照某个content_type下的某个字段排序(本次可以用到)
GET https://cdn.contentful.com/spaces/space_id/entries?access_token=access_token&order=attribute

其中：  
space id：liy6j2tsuhdw  
token：32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e
content_type：company_news

request  

	curl --include \
	'https://cdn.contentful.com/spaces/cfexampleapi/entries?access_token=b4c0n73n7fu1&order=sys.createdAt'
	curl --include \
	'https://cdn.contentful.com/spaces/liy6j2tsuhdw/entries?access_token=32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e&content_type=company_news&order=fields.pub_date'
如果按某个字段排序，则需指定content_type；  
如果是倒序排列则为order=-fields.pub_date。

###2.6 全文搜索（在所有文本中查找关键字）
GET https://cdn.contentful.com/spaces/space_id/entries?access_token=access_token&query=value

其中：  
space id：liy6j2tsuhdw  
token：32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e  
value：聚合

request  （此处的space id 、token已经是真实的值）

	curl --include \
	'https://cdn.contentful.com/spaces/liy6j2tsuhdw/entries?access_token=32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e&query=聚合'


 respond
 
 	{
	  "sys": {
	    "type": "Array"
	  },
	  "total": 5,
	  "skip": 0,
	  "limit": 100,
	  "items": [
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "5AF4kWZbDGSOGG4WAOeKeu",
	        "type": "Entry",
	        "createdAt": "2016-07-06T08:39:28.793Z",
	        "updatedAt": "2016-07-06T08:39:28.793Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "industry_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "行业新闻",
	        "news_title": "让大数据给公众健康带来获得感",
	        "pub_media": "光明日报",
	        "pub_date": "2016-07-05T00:00+08:00",
	        "news_content1": "过度诊疗困扰病患、排队约号费时费力、医疗信息不共享导致重复诊断……未来，医疗领域的这些“短板”将有望逐步补齐。国务院办公厅日前印发《关于促进和规范健康医疗大数据应用发展的指导意见》，提出2020年初步形成健康医疗大数据产业体系。\n在公众看来，健康医疗大数据与百姓生活距离遥远，主要存在于有关部门的数据库当中，运用于宏观层面，难以让实惠变得可见和可及。不少医疗数据也的确如此，比如疾病谱、药品销量、诊疗人数及趋势、医疗筹资和使用数据等，这些宏观数据虽然极具利用价值，却远离百姓生活。\n然而，不少健康医疗大数据其实与百姓生活联系十分紧密，广泛存在于普通诊疗活动当中。例如网上挂号、移动支付、远程会诊、可穿戴医疗设备、就诊“一卡通”等，都是数据信息在诊疗过程中的成功运用，不仅方便了患者，而且提高了医疗资源的利用率。但总体而言，与这方面存在的巨大潜力相比，“数据亲民”还做得远远不够，百姓所得到的实惠也远不及预期，还有很大的发展空间。\n首先，大数据应加强知识普及，为患者提供更全面更准确的医疗信息。当前，医患双方信息不对称的现象突出，主要原因是患者获取信息的渠道不通畅。信息时代理应让患者获取更权威的医疗信息和医学知识，但当前网上信息鱼龙混杂，不仅参考价值不大，有时反而起反作用。大数据理应抢占知识和舆论高点，为提升全民健康素养、填平医患“信息鸿沟”作贡献。\n其次，大数据要更好地匹配医疗供需，帮助化解“看病难”。在这方面，大数据可施展的空间巨大，譬如，可开发出更多便捷式医疗设备，建立更加通畅的医疗数据输送渠道，让医患不见面也能及时反映病情变化，给予精准诊疗。再如，诊疗流程应该得到进一步优化，让等待的时间更短、过程更舒适，使看病成为轻松事。数据可便民，只需让数据多“跑腿”，患者就能少折腾。\n此外，大数据可成为“降费先锋”。当前，检查之所以要反复做，是因为数据没有相互利用，结果不能共享；医疗成本之所以居高不下，与医院重复建设和设备利用率不高有关；网售处方药之所以难实施，是因为规则还没有完善，等等。大数据本可大幅降低医疗费用，但当前在这方面做得还很不够。\n",
	        "news_content2": "     可见，健康医疗大数据的巨大潜力有待挖掘，这既需要突破观念和技术束缚，也需要明确数据的应用模式，既要务虚，也要务实，除了要做好宏观利用外，还要用接地气的方式，提高大数据运用的可及性，增强实用性，让百姓感受到大数据带来的更大获得感。"
	      }
	    },
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "7kKB0ijPUsOE00cEESs0GO",
	        "type": "Entry",
	        "createdAt": "2016-07-06T08:32:11.693Z",
	        "updatedAt": "2016-07-06T08:32:11.693Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "company_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "公司资讯",
	        "news_title": "武汉市委常委冯记春一行调研长江大数据交易所",
	        "pub_media": "荆楚网",
	        "reporter": "长江",
	        "pub_date": "2015-12-22T00:00+08:00",
	        "news_pic1": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "5pNFc0aRxKKeiyiqm06oaa"
	          }
	        },
	        "news_content2": "    10月14日上午，武汉市委常委冯记春、武汉市科技局局长吴志振、东湖高新区管委会副主任、未来办主任宋治平以及市委办公厅相关负责人一行莅临长江大数据交易所进行工作调研。",
	        "news_pic2": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "5pNFc0aRxKKeiyiqm06oaa"
	          }
	        },
	        "news_content3": "    长江大数据交易所副总裁温江凌汇报了长江大数据交易所在平台打造，长江大数据产业联盟筹建、交易规则制定、高峰论坛筹备等各项工作。温江凌还就政府数据开放与冯记春常委进行深入探讨，信息技术和互联网的发展带来了数据的爆发式增长，大数据已经成为全球各国驱动经济增长和社会进步的重要基础和战略资源。目前，社会信息资源大部份掌握在政府手中，而且都是高价值数据，如果武汉市政府能率先公开这些数据，将为武汉市经济转型增长释放巨大红利。目前，长江大数据交易所正在起草相关数据需求方案，希望武汉市政府对于该项工作予以大力支持。",
	        "news_content4": "    冯记春常委对长江大数据交易所取得的工作予以肯定。他表示，长交所在短短两个月的时间工作已取得长足进步，工作推进得十分扎实。政府将持续鼓励大数据交易，并对交易产生的税收优惠政策进行征求意见。",
	        "news_source": "荆楚网",
	        "news_editor": "周婵",
	        "status": "审核通过"
	      }
	    },
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "5MFqjkpl9mGG6IAy8Kcwsm",
	        "type": "Entry",
	        "createdAt": "2016-07-06T08:29:54.889Z",
	        "updatedAt": "2016-07-06T08:29:54.889Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "company_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "行业新闻",
	        "news_title": "打造大数据双11 5周年的聚合数据凭什么",
	        "pub_media": "it168",
	        "reporter": "it168",
	        "pub_date": "2016-06-22T00:00+08:00",
	        "news_content1": "　　大数据的热潮，正在席卷全国。根据《2016-2022年中国大数据市场运营态势及投资战略研究报告》显示，国内大数据市场在2015年市场规模超过100亿，2018年将达到258.6亿人民币。\n　　这在一定程度的刺激了中国的大数据创业，据不完全统计，目前国内的大数据公司已超过300个。根据数据客的统计，2015年中国大数据领域的创新公司总共获得近50亿人民币的投资。去年11月，大数据企业级应用服务商——聚合数据，宣布获得了中国文化产业投资基金领投的2.18亿人民币B轮融资。\n　　商业应用背后的数据推手\n　　数据客把目前中国的大数据创业公司分成了三个类别:数据资源、数据技术、行业应用。聚合数据，显然在数据资源类的大数据公司中，是一名佼佼者。\n",
	        "news_pic1": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "2HF4RLx00MSMQUAwAWMimM"
	          }
	        },
	        "news_content2": "　　聚合数据以自有数据为基础，各种便捷服务整合以及第三方数据接入为平台，为互联网开发者提供全行业标准化的API技术支撑服务。例如，可以在全国天气查询，全国汽车违章查询，手机实名认证，手机话费充值等等一系列的应用中，看到聚合数据的身影。聚合数据就是对这些商业应用，通过API的方式，提供数据资源服务，来获取收益。\n　　怎么衡量聚合数据在大数据创业链条中的地位？在公司的A轮和B轮融资中，正在走数据驱动变革的京东金融成为了的坚定的投资方，这可以看成是对聚合数据在大数据和获取和应用能力的认可和长期看好。\n　　目前，聚合数据平台含有的数据覆盖了LBS、金融、电商、教育、公共交通、日常生活等15个大类近200种数据，依托完善的API数据体系，聚合数据平台聚集了55万APP开发者及企业客户，日调用量超2亿次/天。已经形成了在行业中独特的竞争力。\n　　用数据价值驱动创新创业\n　　聚合数据，顾名思义，就是将市场上公开的数据汇聚到一起，提供给开发者和企业，这比单个企业独自寻找数据的成本会低很多。这也是聚合数据的企业价值观：通过数据的聚合与连接，降低数据资源的门槛，并以此驱动创新和创业。",
	        "news_pic2": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "5PG7FmnrckGO2mwyCEagW6"
	          }
	        },
	        "news_content3": "　　可以想象，对于大部分创业者来说，做一家数据公司很难，做一家数据价值的互联网公司更难。比如，原来做全国天气查询的App，是相当复杂的，因为相关部门没有提供全国性的天气数据，各地区的数据源又非常庞杂。需要到处抓取，既不全，又不稳定。但像聚合数据这样的数据企业，目前已提供全国天气数据的API，创业者只需要直接在网站上下单就可以调用了。\n　　能以一种简单的方式实现App里的某个服务已经成为开发者们最好的选择。无论是京东钱包里的加油卡充值还是趣分期的话费充值，对于开发者而言，这些功能实现起来相当简单，只要调用聚合数据的接口就行了，他们可以把更多的精力放在更核心的事上。\n　　在我看来，互联网时代的商业模式想要成功，必须满足两个特点。第一是快速，互联网的业务多数为新业务，不能在前期占领用户，就输了一半。第二是锁定核心业务，比如把IT运维丢在云端，再比如对数据的采集，直接通过API获取，都满足把时间精力锁定在核心业务的方式。说白了，当走上了互联网创业，就要明确分工协作的理念。从这个角度看，聚合数据的定位是非常准确的。\n　　正如聚合数据网站首页所写的slogan一样：一家数据银行，当然这家数据银行，可以同时为自己和客户创造利益价值。\n　　打造中国大数据的“双11”？\n　　今年6月，聚合数据迎来了5周岁生日，本周开始正式开启周年庆活动，全场的数据将会有个十分可观的折扣力度，最低降至3折。",
	        "news_pic3": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "3XJwvPWPu0wCmEiSEa0q8i"
	          }
	        },
	        "news_content4": "　　除此以外，对于提前在平台充值的用户（6月6日即可充值），在活动期（6.20-6.27）还可以在原有低折扣的基础上再享受额外的9折优惠，活动将持续至6月28日，每日平台还会以底价限量放出15类热门接口，移动联通基站、常用快递、银行卡二元素等热门接口都将参与到活动中。\n　　正所谓，数据有价，价值无价。聚合数据5周年的活动，让本来就已经非常平民化的数据API价值更高。正处于创业中的朋友们，可以关注这一活动。我认为，随着聚合数据在数据资源上的逐渐做大，这种周年促销活动，很可能会成为类似天猫双11那样的购物狂欢节，这就是大数据每年的双11。\n　　最后从大数据的全球商业环境来看，美国在2013年大数据领域的新创公司就获得了36亿美元的投资，这远远高于2015年中国的大数据新创公司融资额。可以预期，从中国如此大的数据量和潜力来看，以聚合数据为代表的大数据公司的前景还很广阔。",
	        "news_source": "it168",
	        "news_editor": "it168",
	        "news_seq": 3,
	        "status": "审核通过",
	        "news_auditor": "郭承峰"
	      }
	    },
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "5FLsJGPULCkeMMoIIIGW22",
	        "type": "Entry",
	        "createdAt": "2016-07-07T03:22:57.084Z",
	        "updatedAt": "2016-07-07T03:22:57.084Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "company_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "公司资讯",
	        "news_title": "国家统计局科研所莅临长江大数据交易中心调研",
	        "pub_media": "长江大数据中心网站",
	        "pub_date": "2016-05-06T00:00+08:00",
	        "news_content1": "5月6日，国家统计局科研所所长万东华一行莅临长江大数据交易中心进行调研，听取了长江大数据交易中心关于大数据应用等相关介绍，与长江大数据交易中心就如何通过大数据应用提升社会治理水平、推进社会经济升级转型以及当前大数据产业发展中遇到的难点问题进行了深入探讨。",
	        "news_pic1": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "5FmWRcZUSAsemUSgGgy4IO"
	          }
	        },
	        "news_content2": "\n   长江大数据交易中心CEO李晖指出，在新经济时代，数据是国家最重要的战略资产，广泛的大数据应用是我国未来的趋势，将给社会生产和生活带来许多变革和提升，也必将有助于国家竞争力提升。当前，政府拥有最庞大最全面的数据资源，这些数据的价值如果能够深入挖掘，广泛应用，将发挥出不可估量的作用。\n大数据应用离不开数据源、应用场景和应用工具。大数据应用对数据的需求十分旺盛，但同时也受到数据开放程度的严重制约。从数据源的归属来看可以分为两大类，一是各行业积累的数据，例如BAT、运营商以及各类大数据和互联网企业沉淀了大量的数据，但开放数据对这些企业带来的收益尚不明朗，风险也较大，因此大多数企业采取较为保守的态度。二是政府数据。由于行业数据开放力度不够，政府数据开放和引导则发挥着重要作用。我国各级政府部门的政务平台拥有着最多的数据资源，如果政府在保证数据安全和隐私的前提下，对非隐私数据逐步开放，可以带活整个大数据产业发展。通过政府数据开放带来的红利和示范效应，还能进一步促进各行业数据开放，进而促进整个大数据生态圈的建立。\n",
	        "news_content3": "在数据跨界融合和应用方面，李晖认为，大数据真正价值体现在企业运用多维度的内外部数据资源整合来提升企业管理、效率和决策能力。如果数据只是在某个行业做沉淀，做深度垂直行业自身就能实现，而未来的大数据应用方向应该是借助外部多维度数据源，实现数据的跨界与融合来提升其它行业的效率和决策能力。以最近大热的VR为例，有些VR公司没有选择上市，就是因为市场的融合度不够。一旦VR和市场上视频媒体、视频网站进行充分融合时，就能共享资源，给受众带来全新的体验和感知，支撑这个新兴产业的发展。而现在的大数据还谈不上融合，大量的软件企业围绕着某一个领域在做深度挖掘，而多个领域的数据融合和使用，在技术层面还需要有一定时间沉淀。长江大数据交易中心作为政府支持的平台，正在致力于引领各类数据源、数据应用和工具聚合，探索推动大数据经济快速崛起的道路。",
	        "news_content4": "　国家统计局科研所认为，数据只有进行深度挖掘和开发才能产生出更大的效益，要推动大数据的运用，长江大数据交易中心担负着实现数据聚合和共享的重要使命，同时也肩负着保卫企业与个人隐私、对信息安全进行把关的重要使命。政府部门则需要通过立法和行政手段以及示范效应来全力推动大数据产业发展。长江大数据交易中心自成立以来，结合本地实际，在促进数据交易、推动数据开放、促进数据全要素流通等方面做出了积极、有益的探索，并取得了显著成效，希望长江大数据交易中心再接再厉，与地方政府一道，共同探索出适合本地情况的大数据应用之路！",
	        "status": "审核通过"
	      }
	    },
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "6Q0CgHLE1aamiK8KOGIScu",
	        "type": "Entry",
	        "createdAt": "2016-07-06T08:36:30.977Z",
	        "updatedAt": "2016-07-06T08:36:30.977Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "industry_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "行业新闻",
	        "news_title": "腾讯守护者创“大数据驱动互联网公益”模式",
	        "pub_date": "2016-07-04T00:00+08:00",
	        "news_content1": "日前，腾讯公司助力公安部门打造“全民防骗，要你出手”反电信网络诈骗宣传月公益活动全面展开。全国警方、政府机构、25位明星和百余家企业跨界联合发声，倡导全民防骗，随手举报。其中，明星公益视频推出仅一日播放量就达6000万次，掀起了全民反诈，人人有责的公益热潮。\n \n  腾讯守护者作为腾讯发起的反诈骗公益平台，通过此次反诈骗月活动，进一步验证了大数据在互联网公益时代的驱动力和社会价值——通过自身的大数据运营经验和技术能力，以及众多合作伙伴的力量，连接警方、企业、各界名人，为反信息诈骗互联网公益创造更为便捷的参与方式，汇聚8亿人的力量形成对抗、打击电信网络诈骗的力量。\n \n   行为即公益，互联网技术降低全民公益参与门槛\n \n互联网+”正在颠覆一切，公益慈善也不例外。互联网技术驱动，大大降低了人们参与公益的门槛，不再局限于参与社会公益实践、定向捐赠这种传统的公益行动，为人人随手参与公益创造了便利条件。互联网为公益的发展创造了一个更大的空间，让公益活动的执行变得公开透明，让公益行为变得简单明确。\n \n传统的公益逐渐变得小众、封闭、低效，如今的公益借助互联网便逐步实现了大众化、开放化和高效化。互联网时代，行为即公益。基于社交网络平台，一个积极的呼吁或行为，都能成为影响公益发展的社会正能量。曾经呼吁公众关注ALS的“冰桶挑战”游戏，以其有意义的公益内涵、简单的参与形式和趣味的内容风靡于美国。传至中国，更加受到各领域大佬、明星的追捧，成为当年最火爆的互联网公益事件。由腾讯公益联合数百家公益组织发起的“一起爱”9.9公益日活动，发动全国网民通过小额现金捐赠，步数捐赠、声音捐赠等多种多样的行为参与公益。\n \n腾讯守护者计划公益平台，以及近期联合公安部发起的全民反电信网络诈骗月活动，正是互联网+公益的典型和升级。一方面，连接警方、数十位明星以及百家企业以创意视频、宣言海报、H5互动游戏等形式在全社交平台发声造势，呼吁网友可以从微信，微博等不同形式进行举报。另一方面，守护者打破传统观念，呼吁全民加入到随手公益，打击网络诈骗的阵营当中。",
	        "news_pic1": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "rmbZT1KMy48EAyooeYaa"
	          }
	        },
	        "news_content2": "   聚焦反诈骗，腾讯守护者让随手举报成为习惯\n \n近年，电信网络诈骗发展迅猛，危害人民的切身利益。不仅个人财产受到威胁，由网络诈骗衍生的“黑色产业”发展之猖獗，已经升级到扰乱了社会秩序的层面。然而，经常被人们忽视的问题是：面对信息诈骗选择沉默之时，实际上是对信息诈骗犯罪的“纵容”。\n \n据腾讯反诈骗联合实验室发布的《2015反信息诈骗大数据报告》显示，仅2015年全国接到诈骗信息的人数高达4.38亿，相当于每3人中就有1人接到过诈骗信息，其中有55.17%的网民遇到网络诈骗，这其中觉得“金额不大，懒得处理”和“不知道如何处理”的合计45%之多。\n \n打击电信网络诈骗，是一场大数据的竞赛。“快速地抓捕骗子，除了警方的全力出击外，还需要网民的出手。从接到骗子的第一条诈骗信息开始，就将骗子进行标记或识别，大大助力维护网络安全。这一场数据的竞赛，需要大家的参与，就不会输给骗子。”原公安部反诈骗刑侦专家金大志表示，网民的积极举报将是打击新型网络犯罪的核心力量。\n \n作为连接社会各界的反信息诈骗公益品牌，腾讯守护者计划在将大数据进行开放和共享的同时，不仅有力狙击诈骗，更强调树立一种反诈公益新理念：让随手举报成为一种习惯，在数据的汇集和分析上取得长期的优势，让骗子无处遁形。\n \n随着腾讯守护者首创的“大数据驱动互联网公益”模式发展，通过8亿网民与企业、机构的举报数据集合、分析与共享，腾讯守护者计划的实践已经取得许多突破性进展：据公开报告显示，自今年4月以来，网络诈骗总量下降约35%，来自用户的诈骗投诉总量下降约40%，日均在各种安全风险场景对用户进行几十万次安全提醒 （财产安全提醒、身份确认验证安全提示等）。腾讯守护者计划还携手警方劝阻2.2万余人避免被骗汇款，帮助2.14万名事主快速拦阻被骗资金3.54亿余元，避免、挽回群众损失合计5.34亿元。这些成绩都与互联网随手公益的理念推广，网友的积极参与，密不可分。"
	      }
	    }
	  ],
	  "includes": {
	    "Asset": [
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "2HF4RLx00MSMQUAwAWMimM",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.736Z",
	          "updatedAt": "2016-07-06T08:20:16.736Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "聚合1",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/2HF4RLx00MSMQUAwAWMimM/c3b565a1fa4874d386ea33070a0a168e/______1.jpg",
	            "details": {
	              "size": 20674,
	              "image": {
	                "width": 334,
	                "height": 250
	              }
	            },
	            "fileName": "聚合1.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "3XJwvPWPu0wCmEiSEa0q8i",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.394Z",
	          "updatedAt": "2016-07-06T08:20:16.394Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "聚合3",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/3XJwvPWPu0wCmEiSEa0q8i/774cb639cea438302ebc5db4da0e4cff/______3.jpg",
	            "details": {
	              "size": 24663,
	              "image": {
	                "width": 534,
	                "height": 250
	              }
	            },
	            "fileName": "聚合3.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "5FmWRcZUSAsemUSgGgy4IO",
	          "type": "Asset",
	          "createdAt": "2016-07-07T03:19:38.204Z",
	          "updatedAt": "2016-07-07T03:19:38.204Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "1",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/5FmWRcZUSAsemUSgGgy4IO/9966742f7b6a504c02b5677d143beb88/1.JPG",
	            "details": {
	              "size": 37006,
	              "image": {
	                "width": 640,
	                "height": 425
	              }
	            },
	            "fileName": "1.JPG",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "5PG7FmnrckGO2mwyCEagW6",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.417Z",
	          "updatedAt": "2016-07-06T08:20:16.417Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "聚合2",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/5PG7FmnrckGO2mwyCEagW6/f7d28d16291144113161d732324fc5a1/______2.jpg",
	            "details": {
	              "size": 32545,
	              "image": {
	                "width": 500,
	                "height": 250
	              }
	            },
	            "fileName": "聚合2.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "5pNFc0aRxKKeiyiqm06oaa",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.396Z",
	          "updatedAt": "2016-07-06T08:20:16.396Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "常委冯记春1",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/5pNFc0aRxKKeiyiqm06oaa/b8f642470a8a302b20fa31eb0aac3d2e/_______________1.jpg",
	            "details": {
	              "size": 38481,
	              "image": {
	                "width": 641,
	                "height": 422
	              }
	            },
	            "fileName": "常委冯记春1.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "rmbZT1KMy48EAyooeYaa",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.725Z",
	          "updatedAt": "2016-07-06T08:20:16.725Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "新闻-腾讯守护者创“大数据驱动互联网公益”模式-图1",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/rmbZT1KMy48EAyooeYaa/8a651d9594929eb88d5ecb6fc851c4aa/______-____________________________________________________________-___1.jpg",
	            "details": {
	              "size": 25394,
	              "image": {
	                "width": 503,
	                "height": 297
	              }
	            },
	            "fileName": "新闻-腾讯守护者创“大数据驱动互联网公益”模式-图1.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      }
	    ]
	  }
	}

###2.7 在某个字段内容中搜索关键字
GET https://cdn.contentful.com/spaces/space_id/entries?access_token=access_token&content_type=content_type&fields.field_id%5Bmatch%5D=value

其中：  
space id：liy6j2tsuhdw  
token：32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e  
content_type ：company_news  
field_id  ：news_content1  
value ：聚合  

request  （此处的space id 、token、content_type、field_id 、value已经是真实的值）

	curl --include \
	'https://cdn.contentful.com/spaces/liy6j2tsuhdw/entries?access_token=32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e&content_type=company_news&fields.news_content1%5Bmatch%5D=聚合'


 respond
 
 	{
	  "sys": {
	    "type": "Array"
	  },
	  "total": 2,
	  "skip": 0,
	  "limit": 100,
	  "items": [
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "5MFqjkpl9mGG6IAy8Kcwsm",
	        "type": "Entry",
	        "createdAt": "2016-07-06T08:29:54.889Z",
	        "updatedAt": "2016-07-06T08:29:54.889Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "company_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "行业新闻",
	        "news_title": "打造大数据双11 5周年的聚合数据凭什么",
	        "pub_media": "it168",
	        "reporter": "it168",
	        "pub_date": "2016-06-22T00:00+08:00",
	        "news_content1": "　　大数据的热潮，正在席卷全国。根据《2016-2022年中国大数据市场运营态势及投资战略研究报告》显示，国内大数据市场在2015年市场规模超过100亿，2018年将达到258.6亿人民币。\n　　这在一定程度的刺激了中国的大数据创业，据不完全统计，目前国内的大数据公司已超过300个。根据数据客的统计，2015年中国大数据领域的创新公司总共获得近50亿人民币的投资。去年11月，大数据企业级应用服务商——聚合数据，宣布获得了中国文化产业投资基金领投的2.18亿人民币B轮融资。\n　　商业应用背后的数据推手\n　　数据客把目前中国的大数据创业公司分成了三个类别:数据资源、数据技术、行业应用。聚合数据，显然在数据资源类的大数据公司中，是一名佼佼者。\n",
	        "news_pic1": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "2HF4RLx00MSMQUAwAWMimM"
	          }
	        },
	        "news_content2": "　　聚合数据以自有数据为基础，各种便捷服务整合以及第三方数据接入为平台，为互联网开发者提供全行业标准化的API技术支撑服务。例如，可以在全国天气查询，全国汽车违章查询，手机实名认证，手机话费充值等等一系列的应用中，看到聚合数据的身影。聚合数据就是对这些商业应用，通过API的方式，提供数据资源服务，来获取收益。\n　　怎么衡量聚合数据在大数据创业链条中的地位？在公司的A轮和B轮融资中，正在走数据驱动变革的京东金融成为了的坚定的投资方，这可以看成是对聚合数据在大数据和获取和应用能力的认可和长期看好。\n　　目前，聚合数据平台含有的数据覆盖了LBS、金融、电商、教育、公共交通、日常生活等15个大类近200种数据，依托完善的API数据体系，聚合数据平台聚集了55万APP开发者及企业客户，日调用量超2亿次/天。已经形成了在行业中独特的竞争力。\n　　用数据价值驱动创新创业\n　　聚合数据，顾名思义，就是将市场上公开的数据汇聚到一起，提供给开发者和企业，这比单个企业独自寻找数据的成本会低很多。这也是聚合数据的企业价值观：通过数据的聚合与连接，降低数据资源的门槛，并以此驱动创新和创业。",
	        "news_pic2": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "5PG7FmnrckGO2mwyCEagW6"
	          }
	        },
	        "news_content3": "　　可以想象，对于大部分创业者来说，做一家数据公司很难，做一家数据价值的互联网公司更难。比如，原来做全国天气查询的App，是相当复杂的，因为相关部门没有提供全国性的天气数据，各地区的数据源又非常庞杂。需要到处抓取，既不全，又不稳定。但像聚合数据这样的数据企业，目前已提供全国天气数据的API，创业者只需要直接在网站上下单就可以调用了。\n　　能以一种简单的方式实现App里的某个服务已经成为开发者们最好的选择。无论是京东钱包里的加油卡充值还是趣分期的话费充值，对于开发者而言，这些功能实现起来相当简单，只要调用聚合数据的接口就行了，他们可以把更多的精力放在更核心的事上。\n　　在我看来，互联网时代的商业模式想要成功，必须满足两个特点。第一是快速，互联网的业务多数为新业务，不能在前期占领用户，就输了一半。第二是锁定核心业务，比如把IT运维丢在云端，再比如对数据的采集，直接通过API获取，都满足把时间精力锁定在核心业务的方式。说白了，当走上了互联网创业，就要明确分工协作的理念。从这个角度看，聚合数据的定位是非常准确的。\n　　正如聚合数据网站首页所写的slogan一样：一家数据银行，当然这家数据银行，可以同时为自己和客户创造利益价值。\n　　打造中国大数据的“双11”？\n　　今年6月，聚合数据迎来了5周岁生日，本周开始正式开启周年庆活动，全场的数据将会有个十分可观的折扣力度，最低降至3折。",
	        "news_pic3": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "3XJwvPWPu0wCmEiSEa0q8i"
	          }
	        },
	        "news_content4": "　　除此以外，对于提前在平台充值的用户（6月6日即可充值），在活动期（6.20-6.27）还可以在原有低折扣的基础上再享受额外的9折优惠，活动将持续至6月28日，每日平台还会以底价限量放出15类热门接口，移动联通基站、常用快递、银行卡二元素等热门接口都将参与到活动中。\n　　正所谓，数据有价，价值无价。聚合数据5周年的活动，让本来就已经非常平民化的数据API价值更高。正处于创业中的朋友们，可以关注这一活动。我认为，随着聚合数据在数据资源上的逐渐做大，这种周年促销活动，很可能会成为类似天猫双11那样的购物狂欢节，这就是大数据每年的双11。\n　　最后从大数据的全球商业环境来看，美国在2013年大数据领域的新创公司就获得了36亿美元的投资，这远远高于2015年中国的大数据新创公司融资额。可以预期，从中国如此大的数据量和潜力来看，以聚合数据为代表的大数据公司的前景还很广阔。",
	        "news_source": "it168",
	        "news_editor": "it168",
	        "news_seq": 3,
	        "status": "审核通过",
	        "news_auditor": "郭承峰"
	      }
	    },
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "5FLsJGPULCkeMMoIIIGW22",
	        "type": "Entry",
	        "createdAt": "2016-07-07T03:22:57.084Z",
	        "updatedAt": "2016-07-07T03:22:57.084Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "company_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_type": "公司资讯",
	        "news_title": "国家统计局科研所莅临长江大数据交易中心调研",
	        "pub_media": "长江大数据中心网站",
	        "pub_date": "2016-05-06T00:00+08:00",
	        "news_content1": "5月6日，国家统计局科研所所长万东华一行莅临长江大数据交易中心进行调研，听取了长江大数据交易中心关于大数据应用等相关介绍，与长江大数据交易中心就如何通过大数据应用提升社会治理水平、推进社会经济升级转型以及当前大数据产业发展中遇到的难点问题进行了深入探讨。",
	        "news_pic1": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Asset",
	            "id": "5FmWRcZUSAsemUSgGgy4IO"
	          }
	        },
	        "news_content2": "\n   长江大数据交易中心CEO李晖指出，在新经济时代，数据是国家最重要的战略资产，广泛的大数据应用是我国未来的趋势，将给社会生产和生活带来许多变革和提升，也必将有助于国家竞争力提升。当前，政府拥有最庞大最全面的数据资源，这些数据的价值如果能够深入挖掘，广泛应用，将发挥出不可估量的作用。\n大数据应用离不开数据源、应用场景和应用工具。大数据应用对数据的需求十分旺盛，但同时也受到数据开放程度的严重制约。从数据源的归属来看可以分为两大类，一是各行业积累的数据，例如BAT、运营商以及各类大数据和互联网企业沉淀了大量的数据，但开放数据对这些企业带来的收益尚不明朗，风险也较大，因此大多数企业采取较为保守的态度。二是政府数据。由于行业数据开放力度不够，政府数据开放和引导则发挥着重要作用。我国各级政府部门的政务平台拥有着最多的数据资源，如果政府在保证数据安全和隐私的前提下，对非隐私数据逐步开放，可以带活整个大数据产业发展。通过政府数据开放带来的红利和示范效应，还能进一步促进各行业数据开放，进而促进整个大数据生态圈的建立。\n",
	        "news_content3": "在数据跨界融合和应用方面，李晖认为，大数据真正价值体现在企业运用多维度的内外部数据资源整合来提升企业管理、效率和决策能力。如果数据只是在某个行业做沉淀，做深度垂直行业自身就能实现，而未来的大数据应用方向应该是借助外部多维度数据源，实现数据的跨界与融合来提升其它行业的效率和决策能力。以最近大热的VR为例，有些VR公司没有选择上市，就是因为市场的融合度不够。一旦VR和市场上视频媒体、视频网站进行充分融合时，就能共享资源，给受众带来全新的体验和感知，支撑这个新兴产业的发展。而现在的大数据还谈不上融合，大量的软件企业围绕着某一个领域在做深度挖掘，而多个领域的数据融合和使用，在技术层面还需要有一定时间沉淀。长江大数据交易中心作为政府支持的平台，正在致力于引领各类数据源、数据应用和工具聚合，探索推动大数据经济快速崛起的道路。",
	        "news_content4": "　国家统计局科研所认为，数据只有进行深度挖掘和开发才能产生出更大的效益，要推动大数据的运用，长江大数据交易中心担负着实现数据聚合和共享的重要使命，同时也肩负着保卫企业与个人隐私、对信息安全进行把关的重要使命。政府部门则需要通过立法和行政手段以及示范效应来全力推动大数据产业发展。长江大数据交易中心自成立以来，结合本地实际，在促进数据交易、推动数据开放、促进数据全要素流通等方面做出了积极、有益的探索，并取得了显著成效，希望长江大数据交易中心再接再厉，与地方政府一道，共同探索出适合本地情况的大数据应用之路！",
	        "status": "审核通过"
	      }
	    }
	  ],
	  "includes": {
	    "Asset": [
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "2HF4RLx00MSMQUAwAWMimM",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.736Z",
	          "updatedAt": "2016-07-06T08:20:16.736Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "聚合1",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/2HF4RLx00MSMQUAwAWMimM/c3b565a1fa4874d386ea33070a0a168e/______1.jpg",
	            "details": {
	              "size": 20674,
	              "image": {
	                "width": 334,
	                "height": 250
	              }
	            },
	            "fileName": "聚合1.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "3XJwvPWPu0wCmEiSEa0q8i",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.394Z",
	          "updatedAt": "2016-07-06T08:20:16.394Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "聚合3",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/3XJwvPWPu0wCmEiSEa0q8i/774cb639cea438302ebc5db4da0e4cff/______3.jpg",
	            "details": {
	              "size": 24663,
	              "image": {
	                "width": 534,
	                "height": 250
	              }
	            },
	            "fileName": "聚合3.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "5FmWRcZUSAsemUSgGgy4IO",
	          "type": "Asset",
	          "createdAt": "2016-07-07T03:19:38.204Z",
	          "updatedAt": "2016-07-07T03:19:38.204Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "1",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/5FmWRcZUSAsemUSgGgy4IO/9966742f7b6a504c02b5677d143beb88/1.JPG",
	            "details": {
	              "size": 37006,
	              "image": {
	                "width": 640,
	                "height": 425
	              }
	            },
	            "fileName": "1.JPG",
	            "contentType": "image/jpeg"
	          }
	        }
	      },
	      {
	        "sys": {
	          "space": {
	            "sys": {
	              "type": "Link",
	              "linkType": "Space",
	              "id": "liy6j2tsuhdw"
	            }
	          },
	          "id": "5PG7FmnrckGO2mwyCEagW6",
	          "type": "Asset",
	          "createdAt": "2016-07-06T08:20:16.417Z",
	          "updatedAt": "2016-07-06T08:20:16.417Z",
	          "revision": 1,
	          "locale": "zh-CN"
	        },
	        "fields": {
	          "title": "聚合2",
	          "file": {
	            "url": "//images.contentful.com/liy6j2tsuhdw/5PG7FmnrckGO2mwyCEagW6/f7d28d16291144113161d732324fc5a1/______2.jpg",
	            "details": {
	              "size": 32545,
	              "image": {
	                "width": 500,
	                "height": 250
	              }
	            },
	            "fileName": "聚合2.jpg",
	            "contentType": "image/jpeg"
	          }
	        }
	      }
	    ]
	  }
	}


###2.8 查询完全匹配字段的内容
GEThttps://cdn.contentful.com/spaces/space_id/entries?access_token=access_token&content_type=content_type&fields.field_id=value

其中：  
space id：liy6j2tsuhdw  
token：32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e  
content_type：industry_news  
field_id：pub_media  
value：半月谈  

request  

	curl --include \
	'ttps://cdn.contentful.com/spaces/liy6j2tsuhdw/entries?access_token=32b3c28cf2b53a576d5329bf69bb125da34c02f0934e0e773fecc6cc8f78819e&content_type=industry_news&fields.pub_media=半月谈'

 respond
 
	{
	  "sys": {
	    "type": "Array"
	  },
	  "total": 1,
	  "skip": 0,
	  "limit": 100,
	  "items": [
	    {
	      "sys": {
	        "space": {
	          "sys": {
	            "type": "Link",
	            "linkType": "Space",
	            "id": "liy6j2tsuhdw"
	          }
	        },
	        "id": "3GVVQs69Dy8IegAm86UqSU",
	        "type": "Entry",
	        "createdAt": "2016-07-14T03:20:50.729Z",
	        "updatedAt": "2016-07-14T03:20:50.729Z",
	        "revision": 1,
	        "contentType": {
	          "sys": {
	            "type": "Link",
	            "linkType": "ContentType",
	            "id": "industry_news"
	          }
	        },
	        "locale": "zh-CN"
	      },
	      "fields": {
	        "news_id": 201607140001,
	        "news_type": "行业新闻",
	        "news_title": "大数据,助消防精准救援“智慧”灭火 -湖北宜昌“智慧消防”创新实践观察 ",
	        "pub_media": "新华每日电讯",
	        "reporter": "邹伟　李鹏翔",
	        "pub_date": "2016-07-01T08:42+08:00",
	        "news_content1": "湖北省宜昌市以“智慧城市”建设试点为契机,探索建设“智慧消防”,借助科技的力量实现火灾防控和灭火救援能力质的提升。其创新理念和经验做法,或可为大数据时代下提升社会治理水平提供有益的启示。\n\n据新华社湖北电(记者邹伟　李鹏翔)大数据、云计算……这些尖端科技和消防安全工作有什么关系?湖北省宜昌市以“智慧城市”建设试点为契机,探索建设“智慧消防”,借助科技的力量实现火灾防控和灭火救援能力质的提升。其创新理念和经验做法,或可为大数据时代下提升社会治理水平提供有益的启示。\n\n全域采集,摸清火灾隐患“家底”\n\n“格格”,这是伍家岗区宝塔河街道东星社区居民们对网格员邓倩茜亲切的称呼。\n\n　　宜昌实行网格化管理,全市175个社区、1436个村划分为1.1万个网格,每个网格配备1名专职网格员。网格员的日常工作就是辖区巡查走访,了解社情民意、服务社区居民。\n\n　　“消防工作被嵌入网格管理体系之中,信息采集、隐患排查、宣传教育、应急救助也是我们职责的一部分。”邓倩茜说,哪个消火栓有损坏、哪条消防通道被堵了、哪家有腿脚不方便的老人,都在她采集的信息之列,通过安装在手机上的“社区E通”消防安全模块,第一时间向全市联网的信息平台上报。\n\n　　“‘智慧消防’的核心是消防信息化,前提是全面立体的消防管理信息数据库,即‘消防大数据’。”宜昌市公安消防支队副支队长冯明静向记者表示。\n\n　　“消防大数据的来源中,有1.1万名网格员的日常采集,有消防部门内部业务系统的对接融合,还有与全市各个部门的互联互通。”冯明静进一步介绍,依托宜昌市政府建设的“三峡云计算中心”,可打破部门壁垒,共享公安、房管、工商、气象、教育、卫生等部门的基础信息数据库,实现人口、房屋、法人、气象等11类信息实时联通,形成统一的消防信息资源体系,一键查询、随用随调。\n\n　　目前,“智慧消防”系统已汇聚家庭信息数据43万条、城市部件数据33万条、消防业务数据7.7万条,地下管网数据8316条,危化品单位数据6154条；同时,运用比对、归类、清洗等技术手段,每分每秒不断刷新数据,实现对消防工作信息的真实、有效、实时掌握。\n\n　　统计显示,2015年宜昌市受理的20多万件火灾隐患中,90%在基层化解,仅有10%需消防部门督办整改,做到“小隐患不出网格,一般隐患不出社区”。\n\n防消互联,火场救援更加精准高效\n\n　　“报告指挥长,系统检测到市区某医院住院部二楼有火警,请指示!”\n\n　　“请迅速核实现场情况!”\n\n　　在宜昌市消防安全远程监控中心,一场灭火救援演练正在紧张有序进行中。\n\n　　记者从监控中心的大屏幕上看到,接报火警的第一时间,事发地点已被精准定位,周边建筑、人口、消火栓、重点单位、摄像头、消防力量等一并显现。\n\n　　随即,指挥长确定现场灾害等级,确认系统生成的调度方案,一键式推送至临近现役消防部队、消防执勤点、政府和企业专职消防队等应急救援力量。系统实时定位赶往火场的消防车辆,根据交通拥堵情况拟制出一条最佳线路,消防人员和车辆装备在第一时间抵达事故现场,一场火灾被成功消灭在初起阶段。\n\n　　“有了‘智慧消防’系统,我们出警救援心里更有底,在浓烟里走的时候方向感、目标感更强。”宜昌市公安消防支队紫阳中队指导员王钰崧说,消防官兵在赶往火场的路上就可全面了解建筑内部人员情况、有无易燃易爆危险源、消防设施运行状况等,为灭火救援指挥决策提供依据,堪称官兵们的“千里眼”和“顺风耳”。\n\n　　“各类数据的快速查询和整合为火场指挥员提供了强大的信息支撑,构建了灭火救援的全方位、多角度可视化指挥,从而提高了作战效能。”宜昌市公安消防支队支队长金建立表示,大数据技术的运用,既能极大地提高搜救被困人员的成功率,也能极大地降低官兵伤亡风险,使得消防救援更加精准、高效。\n"
	      }
	    }
	  ]

	
 ### 
